#!/bin/bash
set -e
mvn clean compile assembly:single
docker build --tag tokenmanager-service .
docker image prune -f