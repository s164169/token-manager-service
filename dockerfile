FROM adoptopenjdk:8-jre-hotspot
COPY target/tokenmanager_service-jar-with-dependencies.jar /usr/src/
WORKDIR /usr/src/
CMD java -Djava.net.preferIPv4Stack=true\
         -Djava.net.preferIPv4Addresses=true\
        -jar tokenmanager_service-jar-with-dependencies.jar
