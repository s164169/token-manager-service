#//@author Mathias Boss Jørgensen
Feature: Token Manager Feature

  Scenario: Transfer Money From To
    Given a paymentDTO
    When the service receives a "transferMoneyFromTo" transfer event
    Then the dto is validated
    Then the "transferMoneyFromToValidated" event is broadcast Transfer

  Scenario: Delete Customer
    Given a customer
    When the service receives a "DeleteCustomer" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then the Customer is deleted

  Scenario: Create customer
    Given a customer
    When the service receives a "CreateCustomer" customer event
    Then the customer is created

  Scenario: Update Customer
    Given a customer
    When the service receives a "UpdateCustomer" customer event
    Then the customer is updated

  Scenario: Get customer tokens
    Given a customer ID with the ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    When the service receives a "CostumerAvailableTokens" event with ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8"
    Then the number of tokens for the ID "ffa33c5d-4cf5-451d-81da-4d5242feabd8" is counted
    Then the "CostumerAvailableTokensSucceeded" customer event is broadcast

  Scenario: Request Tokens
    Given a customer ID with the ID "b982f785-09c2-4942-ae14-9a04fe21e8d9"
    When the service receives a "RequestTokens" request event with ID "b982f785-09c2-4942-ae14-9a04fe21e8d9" and 1
    Then the token request with ID "b982f785-09c2-4942-ae14-9a04fe21e8d9" is validated 1
    Then 1 number of tokens are generated for the ID "b982f785-09c2-4942-ae14-9a04fe21e8d9"
    Then the "RequestTokensSucceeded" token event is broadcast

  Scenario: Validate Token
    Given a token with "b982f785-09c2-4942-ae14-9a04fe21e8d8"
    When the service receives a "ValidateToken" token event
    Then the service validates the token
    Then the "ValidateTokenSucceeded" validation event is broadcast

  Scenario: Refund Payment
    Given a paymentDTO
    When the service receives a "RefundPayment" event
    Then the dto is validated
    Then the "RefundPaymentValidated" event is broadcast Transfer