package behaviourtests;

import cucumber.api.CucumberOptions;
import cucumber.api.SnippetType;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;
//@author Sebatian Fischer
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources",
        plugin = { "html:target/cucumber/wikipedia.html"},
        monochrome=true,
        snippets = SnippetType.CAMELCASE)
public class CucumberTest {
}
