package behaviourtests;

import com.dtupay.tokenservice.DAL.Abstractions.ITokenManager;
import com.dtupay.tokenservice.TokenManagerServiceReceiver;
import cucumber.api.PendingException;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Merchant;
import dtupay.core.DAL.models.Token;
import dtupay.core.DAL.models.TransactionRequest;
import dtupay.core.models.DTOs.PaymentDTO;
import dtupay.core.models.DTOs.TransferMoneyDTO;
import gherkin.lexer.Pa;
import messaging.Event;
import messaging.EventSender;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static org.mockito.Mockito.*;
//@author Mathias Bo Jensen
public class TokenManagerSteps {

    ITokenManager tokenManager = mock(ITokenManager.class);

    EventSender eventSender = mock(EventSender.class);

    private TokenManagerServiceReceiver tokenManagerServiceReceiver = new TokenManagerServiceReceiver(tokenManager, eventSender);
    private Customer customer;
    private PaymentDTO paymentDTO;

    private Token token;
    private List<Token> tokenList;

    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
    }

    @Then("^the \"([^\"]*)\" event is broadcast$")
    public void theEventIsBroadcast(String eventType) throws Throwable {
        Object[] args = {};
        Event event = new Event(eventType, args);
        verify(eventSender).sendEvent(event);
    }

    @When("^the service receives a \"([^\"]*)\" customer event$")
    public void theServiceReceivesACustomerEvent(String arg0) throws Throwable {
        Object[] args =  {customer} ;
        Event event = new Event(arg0, args);
        tokenManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the customer is updated$")
    public void theCustomerIsUpdated() {
        verify(tokenManager).updateCustomer(any(Customer.class));
    }

    @Given("^a customer$")
    public void aCustomer() {
        customer = new Customer("test1","test1","test1",UUID.randomUUID(),"test1");
        doNothing().when(tokenManager).createCustomer(any(Customer.class));
        doNothing().when(tokenManager).updateCustomer(any(Customer.class));
    }
    @Then("^the Customer is deleted$")
    public void theCustomerIsDeleted() {
        verify(tokenManager).deleteCustomer(any(UUID.class));
    }
    @Then("^the customer is created$")
    public void theCustomerIsCreated() {
        verify(tokenManager).createCustomer(any(Customer.class));
    }

    @Given("^a paymentDTO$")
    public void aPaymentDTO() {
        paymentDTO = new PaymentDTO();
        paymentDTO.setTokenID("testTokenID");
        when(tokenManager.validateToken(Mockito.anyString())).thenReturn(true);
    }

    @When("^the service receives a \"([^\"]*)\" transfer event$")
    public void theServiceReceivesATransferEvent(String arg0) throws Throwable {
        Object[] args =  {paymentDTO} ;
    Event event = new Event(arg0, args);
        tokenManagerServiceReceiver.receiveEvent(event);
    }


    @Given("^a customer ID with the ID \"([^\"]*)\"$")
    public void aCustomerIDWithTheID(String arg0) throws Throwable {
        customer = new Customer("test","test","test",UUID.fromString(arg0),"ttest");
        tokenList = new ArrayList<Token>();
        when(tokenManager.getCustomersNumberOfTokens(UUID.fromString(arg0))).thenReturn(1);
        when(tokenManager.validateToken(any(String.class))).thenReturn(true);
        when(tokenManager.checkForExceedingTokenAmount(any(UUID.class),anyInt())).thenReturn(false);
        when(tokenManager.generateTokens(any(UUID.class),anyInt())).thenReturn(tokenList);

    }

    @When("^the service receives a \"([^\"]*)\" event with ID \"([^\"]*)\"$")
    public void theServiceReceivesAEventWithArgs(String arg0, String arg1) throws Throwable {
        Object[] args =  {UUID.fromString(arg1)} ;
        Event event = new Event(arg0, args);
        tokenManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the number of tokens for the ID \"([^\"]*)\" is counted$")
    public void theNumberOfTokensForTheIDIsCounted(String arg0) throws Throwable {
        verify(tokenManager).getCustomersNumberOfTokens(customer.getUserID());
    }


    @Then("^the \"([^\"]*)\" customer event is broadcast$")
    public void theCustomerEventIsBroadcast(String arg0) throws Throwable {
        Object[] args = {1};
        Event event = new Event(arg0, args);
        verify(eventSender).sendEvent(refEq(event));
    }

    @Then("^the \"([^\"]*)\" event is broadcast Transfer$")
    public void theEventIsBroadcastTransfer(String arg0) throws Throwable {
        Object[] args = {paymentDTO};
        Event event = new Event(arg0, args);
        verify(eventSender).sendEvent(refEq(event));
    }

    @Then("^the dto is validated$")
    public void theDtoIsValidated() {
        verify(tokenManager).validateToken(paymentDTO.getTokenID());
    }

    @Given("^a token with \"([^\"]*)\"$")
    public void aTokenWith(String arg0) {
        token = new Token("test",UUID.fromString(arg0));
        when(tokenManager.validateToken(token.getTokenID())).thenReturn(true);
    }

    @When("^the service receives a \"([^\"]*)\" token event$")
    public void theServiceReceivesATokenEvent(String arg0) throws Throwable {
        Object[] args =  {token} ;
        Event event = new Event(arg0, args);
        tokenManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the service validates the token$")
    public void theServiceValidatesTheToken() {
        verify(tokenManager).validateToken(token.getTokenID());
    }

    @When("^the service receives a \"([^\"]*)\" event$")
    public void theServiceReceivesAEvent(String arg0) throws Throwable {
        Object[] args =  {paymentDTO} ;
        Event event = new Event(arg0, args);
        tokenManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the \"([^\"]*)\" token event is broadcast$")
    public void theTokenEventIsBroadcast(String arg0) throws Throwable {
        Object[] args = {tokenList};
        Event event = new Event(arg0, args);
        verify(eventSender).sendEvent(refEq(event));
    }

    @When("^the service receives a \"([^\"]*)\" request event with ID \"([^\"]*)\" and (\\d+)$")
    public void theServiceReceivesARequestEventWithIDAnd(String arg0, String arg1, int arg2) throws Throwable {
        Object[] args =  {UUID.fromString(arg1),arg2} ;
        Event event = new Event(arg0, args);
        tokenManagerServiceReceiver.receiveEvent(event);
    }

    @Then("^the token request with ID \"([^\"]*)\" is validated (\\d+)$")
    public void theTokenRequestWithIDIsValidated(String arg0, int arg1) throws Throwable {
        verify(tokenManager).checkForExceedingTokenAmount(UUID.fromString(arg0),arg1);
    }

    @Then("^(\\d+) number of tokens are generated for the ID \"([^\"]*)\"$")
    public void numberOfTokensAreGeneratedForTheID(int arg0, String arg1) throws Throwable {
        verify(tokenManager).generateTokens(UUID.fromString(arg1),arg0);
    }

    @Then("^the \"([^\"]*)\" validation event is broadcast$")
    public void theValidationEventIsBroadcast(String arg0) throws Throwable {
        Object[] args = {true};
        Event event = new Event(arg0, args);
        verify(eventSender).sendEvent(refEq(event));
    }
}
