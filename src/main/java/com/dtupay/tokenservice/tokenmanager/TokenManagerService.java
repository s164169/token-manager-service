package com.dtupay.tokenservice.tokenmanager;

import com.dtupay.tokenservice.DAL.Abstractions.ITokenRepository;
import com.dtupay.tokenservice.DAL.Abstractions.ICustomerRepository;
import com.dtupay.tokenservice.DAL.Abstractions.ITokenManager;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Token;
import dtupay.core.Enums.TokenStatus;

import java.util.*;
//@author William Embarek
public class TokenManagerService implements ITokenManager {
    private ITokenRepository tokenRepository;
    private ICustomerRepository customerRepository;

    public TokenManagerService(ITokenRepository tokenRepository, ICustomerRepository customerRepository) {
        this.tokenRepository = tokenRepository;
        this.customerRepository = customerRepository;
    }

    public List<Token> requestTokens(UUID customerID, int requestedNumberOfTokens) {
        int numberOfCustomerTokens = customerRepository.getCustomerByID(customerID).numberOfTokens();
        if(numberOfCustomerTokens > 1 || requestedNumberOfTokens > 5) {
            return Collections.emptyList();
        } else {
            return generateTokens(customerID, requestedNumberOfTokens);
        }
    }

    @Override
    public int getCustomersNumberOfTokens(UUID customerID) {
        return customerRepository.getCustomerByID(customerID).numberOfTokens();
    }

    //TODO: Change name / method.
    @Override
    public boolean validateToken(String tokenID) {
        Token token = tokenRepository.getTokenByID(tokenID);
        if(!doesTokenExist(token))
            return false;

        if (isTokenUsed(token.getTokenID())){
            return false;
        }

        return true;
    }

    @Override
    public boolean isRequestedGenerationAllowed(UUID customerId, int numberOfRequestedTokens) {
        int AmountOfTokens = tokenRepository.getAmountAllUnusedTokens(customerId);

        if ((AmountOfTokens > 1) || (numberOfRequestedTokens > 5)){
            return true;
        }

        return false;
    }

    private boolean doesTokenExist(Token token) {
        return tokenRepository.doesTokenExist(token);
    }

    @Override
    public boolean useToken(Token token) {
        if(!validateToken(token.getTokenID())) return false;
        token.setStatus(TokenStatus.ACCEPTED); // Temporary
        tokenRepository.updateToken(token);
        return true;
    }

    public void removeToken(String tokenId) {
        tokenRepository.removeToken(tokenId);
    }

    //TODO: Change to return type Optional(Token Repo) & change input to tokenID (String) instead of Token.
    public boolean wasAccepted(String tokenID) {
        Token dbToken = tokenRepository.getTokenByID(tokenID);
        if(dbToken == null){
            return false;
        }
        return dbToken.getStatus() == TokenStatus.ACCEPTED;
    }

    @Override
    public List<Token> generateTokens(UUID customerID, int numberOfTokens){
        ArrayList<Token> generatedTokens = new ArrayList<>();
        String uuid;
        for(int i = 0; i < numberOfTokens; i++){
            uuid = UUID.randomUUID().toString();
            Token newToken = new Token(uuid, customerID);
            newToken.setStatus(TokenStatus.UNUSED);
            generatedTokens.add(newToken);
            tokenRepository.addToken(newToken);
        }//TODO: Change to return type Optional(Token Repo).
        return generatedTokens;
    }

    //TODO: Change to tokenID.
    public boolean isTokenUsed(String tokenID) {
        Token dbToken = tokenRepository.getTokenByID(tokenID);
        return dbToken.getStatus() != TokenStatus.UNUSED;
    }

    @Override
    public void deleteCustomer(UUID uuid) {
        customerRepository.deleteCustomer(uuid);
    }

    @Override
    public void createCustomer(Customer customer) {
        customerRepository.createCustomer(customer);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customerRepository.updateCustomer(customer);
    }

}
