package com.dtupay.tokenservice;

import com.dtupay.tokenservice.DAL.InMemoryRepository;
import com.dtupay.tokenservice.tokenmanager.TokenManagerService;
import dtupay.core.DAL.models.Token;
import messaging.EventSender;
import messaging.rabbitmq.RabbitMqListener;
import messaging.rabbitmq.RabbitMqSender;
//@author Frederik Kirkegaard
public class StartUp {

    public static void main(String[] args) throws Exception{
        new StartUp().startUp();
    }

    private void startUp() throws Exception {
        Token token = new Token("db2e11cd-a00f-46db-855c-e443e561e303", null); //TODO: This is shit, create customer that owns the token.
        InMemoryRepository inMemoryRepository = new InMemoryRepository();
        inMemoryRepository.addToken(token);


        EventSender eventSender = new RabbitMqSender();
        TokenManagerService tokenManager = new TokenManagerService(inMemoryRepository, inMemoryRepository);
        TokenManagerServiceReceiver tokenManagerServiceReceiver = new TokenManagerServiceReceiver(tokenManager, eventSender);
        new RabbitMqListener(tokenManagerServiceReceiver).listen();
    }
}
