package com.dtupay.tokenservice;

import com.dtupay.tokenservice.DAL.Abstractions.ITokenManager;
import com.dtupay.tokenservice.DAL.Abstractions.ITokenRepository;
import com.dtupay.tokenservice.tokenmanager.TokenManagerService;
import com.google.gson.Gson;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Token;
import dtupay.core.models.DTOs.PaymentDTO;
import messaging.Event;
import messaging.EventReceiver;
import messaging.EventSender;

import java.util.List;
import java.util.UUID;
//@author Magnus Glasdam Jakobsen
public class TokenManagerServiceReceiver implements EventReceiver {
    private EventSender sender;
    private ITokenManager tokenManager;

    //Ny version
    private static final String TRANSFER_MONEY_FROM_TO = "transferMoneyFromTo";
    private static final String TRANSFER_MONEY_FROM_TO_VALIDATED = "transferMoneyFromToValidated";
    private static final String DELETE_CUSTOMER = "DeleteCustomer";
    private static final String CREATE_CUSTOMER = "CreateCustomer";
    private static final String UPDATE_CUSTOMER = "UpdateCustomer";


    private static final String COSTUMER_AVAILABLE_TOKENS = "CostumerAvailableTokens";
    private static final String COSTUMER_AVAILABLE_TOKENS_SUCCEEDED = "CostumerAvailableTokensSucceeded";

    private static final String REQUEST_TOKENS = "RequestTokens";
    private static final String REQUEST_TOKENS_SUCCEEDED = "RequestTokensSucceeded";
    private static final String REQUEST_TOKENS_FAILED = "RequestTokensFailed";

    private static final String VALIDATE_TOKEN = "ValidateToken";
    private static final String VALIDATE_TOKEN_SUCCEEDED = "ValidateTokenSucceeded";

    private static final String REFUND_PAYMENT = "RefundPayment";
    private static final String REFUND_PAYMENT_VALIDATED = "RefundPaymentValidated";


    public TokenManagerServiceReceiver(ITokenManager tokenManager, EventSender sender) {
        this.tokenManager = tokenManager;
        this.sender = sender;
    }

    @Override
    public void receiveEvent(Event event) throws Exception {
        Gson gson = new Gson();
        Object[] args = event.getArguments();
        switch (event.getEventType()) {
            //Ny version
            case TRANSFER_MONEY_FROM_TO:
                System.out.println("Handling event: " + event);
                PaymentDTO paymentDTO = gson.fromJson(gson.toJson(args[0]), PaymentDTO.class);
                if (!tokenManager.validateToken(paymentDTO.tokenID)) throw new Exception("Token not valid");
                sendEvent(TRANSFER_MONEY_FROM_TO_VALIDATED, args);
                break;
            case DELETE_CUSTOMER:
                System.out.println("Handling event: " + event);
                tokenManager.deleteCustomer(gson.fromJson(gson.toJson(args[0]), UUID.class));
                break;
            case CREATE_CUSTOMER:
                System.out.println("Handling event: " + event);
                tokenManager.createCustomer(gson.fromJson(gson.toJson(args[0]), Customer.class));
                break;
            case UPDATE_CUSTOMER:
                System.out.println("Handling event: " + event);
                tokenManager.updateCustomer(gson.fromJson(gson.toJson(args[0]), Customer.class));
                break;

            case COSTUMER_AVAILABLE_TOKENS:
                System.out.println("Handling event: " + event);
                UUID customerIdForNumberOfTokens = gson.fromJson(gson.toJson(args[0]), UUID.class);
                int numberOfCustomerTokens = tokenManager.getCustomersNumberOfTokens(customerIdForNumberOfTokens);
                sendEvent(COSTUMER_AVAILABLE_TOKENS_SUCCEEDED, new Object[] {numberOfCustomerTokens});
                break;

            case REQUEST_TOKENS: // TODO: This is not done
                System.out.println("Handling event: " + event);
                UUID customerIdToGenerateTokens = gson.fromJson(gson.toJson(args[0]), UUID.class);
                int numberOfRequestedTokens = gson.fromJson(gson.toJson(args[1]), Integer.class);

                if (tokenManager.isRequestedGenerationAllowed(customerIdToGenerateTokens, numberOfRequestedTokens)) {
                    sendEvent(REQUEST_TOKENS_FAILED);
                    break;
                }

                List<Token> listOfTokens = tokenManager.generateTokens(customerIdToGenerateTokens, numberOfRequestedTokens);
                sendEvent(REQUEST_TOKENS_SUCCEEDED, new Object[] {listOfTokens});
                break;

            case VALIDATE_TOKEN:
                System.out.println("Handling event: " + event);
                Token tokenToValidate = gson.fromJson(gson.toJson(args[0]), Token.class);
                boolean validationResult = tokenManager.validateToken(tokenToValidate.getTokenID());
                sendEvent(VALIDATE_TOKEN_SUCCEEDED, new Object[] {validationResult});
                break;

            case REFUND_PAYMENT:
                System.out.println("Handling event: " + event);
                PaymentDTO payDTO = gson.fromJson(gson.toJson(args[0]), PaymentDTO.class);
                if (!tokenManager.validateToken(payDTO.tokenID)) throw new Exception("Token not valid");
                sendEvent(REFUND_PAYMENT_VALIDATED, args);
                break;

            default:
                System.out.println("Ignoring " + event);
                break;
        }
    }

    private void sendEvent(String event, Object[] receiveEventResult) throws Exception {
        Event e = new Event(event, receiveEventResult);
        sender.sendEvent(e);
    }

    private void sendEvent(String event) throws Exception {
        Event e = new Event(event);
        sender.sendEvent(e);
    }
}
