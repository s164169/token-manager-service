package com.dtupay.tokenservice.DAL;

import com.dtupay.tokenservice.DAL.Abstractions.ICustomerRepository;
import com.dtupay.tokenservice.DAL.Abstractions.ITokenRepository;
import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Token;


import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
//@author Mathias Boss Jørgensen
public class InMemoryRepository implements ICustomerRepository, ITokenRepository {
    private Map<String, Token> tokens = new HashMap<>();
    private Map<UUID, Customer> customers = new HashMap<>();

    @Override
    public Token getTokenByID(String tokenID) {
        return tokens.get(tokenID);
    }

    @Override
    public void addToken(Token newToken) {
        tokens.put(newToken.getTokenID(), newToken);
    }

    @Override
    public void removeToken(String tokenID) {
        tokens.remove(tokenID);
    }

    @Override
    public boolean doesTokenExist(Token token) {
        return tokens.containsValue(token);
    }

    @Override
    public void updateToken(Token token) {
        tokens.replace(token.getTokenID(), token);
    }

    @Override
    public int getAmountAllUnusedTokens(UUID customerId) {
        return tokens.values().stream().filter(x -> customerId.equals(x.getCustomerID())).collect(Collectors.toList()).size();
    }

    @Override
    public Customer getCustomerByID(UUID customerID) {
        return customers.get(customerID);
    }

    @Override
    public void deleteCustomer(UUID customerID) {
        customers.remove(customerID);
    }

    @Override
    public void createCustomer(Customer customer) {
        customers.put(customer.getUserID(),customer);
    }

    @Override
    public void updateCustomer(Customer customer) {
        customers.replace(customer.getUserID(),customer);
    }


}
