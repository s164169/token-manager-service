package com.dtupay.tokenservice.DAL.Abstractions;

import dtupay.core.DAL.models.Customer;
import dtupay.core.DAL.models.Token;
import java.io.InvalidObjectException;
import java.util.List;
import java.util.UUID;
//@author Sebatian Fischer
public interface ITokenManager {
    List<Token> requestTokens(UUID customerID, int requestedNumberOfTokens);

    int getCustomersNumberOfTokens(UUID customerID);

    boolean useToken(Token token) throws InvalidObjectException;

    boolean wasAccepted(String tokenID);

    boolean isTokenUsed(String tokenID);

    void removeToken(String tokenId);

    public boolean validateToken(String tokenID);

    public void deleteCustomer(UUID uuid);
    public void createCustomer(Customer customer);
    public void updateCustomer(Customer customer);

    List<Token> generateTokens(UUID customerIdToGenerateTokens, int numberOfRequestedTokens);
    boolean isRequestedGenerationAllowed(UUID customerIdToGenerateTokens, int numberOfRequestedTokens);
}
