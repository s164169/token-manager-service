package com.dtupay.tokenservice.DAL.Abstractions;

import dtupay.core.DAL.models.Customer;
import java.util.UUID;
//@author Frederik Kirkegaard
public interface ICustomerRepository {

    Customer getCustomerByID(UUID customerID);

    void deleteCustomer(UUID customerID);

    void createCustomer(Customer customer);

    void updateCustomer(Customer customer);
}
