package com.dtupay.tokenservice.DAL.Abstractions;

import dtupay.core.DAL.models.Token;

import java.util.UUID;
//@author Mathias Bo Jensen
public interface ITokenRepository {

    Token getTokenByID(String tokenID);

    void addToken(Token newToken);

    void removeToken(String tokenID);

    boolean doesTokenExist(Token token);

    void updateToken(Token token);

    int getAmountAllUnusedTokens(UUID customerId);
}
